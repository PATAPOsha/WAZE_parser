import requests
import csv
import time
import sys
import thread
from multiprocessing.dummy import Pool as ThreadPool
from threading import Lock
# from requests.packages.urllib3.exceptions import InsecureRequestWarning
from datetime import datetime

# requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

d ={}
with open('CONFIG.conf') as f:
    for line in f:
        (key, val) = line.split('=')
        d[key] = val

login = d['login'].strip()
passw = d['pass'].strip()
threads = int(d['threads'])
output = 'output\\%s.csv' % str(datetime.now()).split('.')[0].replace('-','.').replace(':','-')

print 'Enter coordinates in the following format: lat, lon   (Example: 59.072812, 1.582339)'
bottom_point = input("Enter first point: ")
bott_y = bottom_point[0] * 1000000
bott_x = bottom_point[1] * 1000000

top_point = input('Enter second point: ')
top_y = top_point[0] * 1000000
top_x = top_point[1] * 1000000


def equal_areas(bott_x, bott_y, top_x, top_y, x_height, y_height):
    l = []
    for x in range(int(bott_x), int(top_x), x_height):
        for y in range(int(bott_y), int(top_y), y_height):
            l.append ([x/(1000000*1.0),y/(1000000*1.0),(x+x_height)/(1000000*1.0), (y+y_height)/(1000000*1.0)])
    return l

data = {
    'language':'ru',
    'bbox':'',
    'cameras':'true',
}

get_head = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
    'X-Requested-With': 'XMLHttpRequest',
    'Accept-Encoding':'gzip, deflate, br',
    'Accept-Language':'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
    'Referer': 'https://www.waze.com/ru/editor/',

}

post_head = {
    'Origin':'https://www.waze.com',
    'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
}

csv_head = ['IDX', 'Direction', 'TYPE', 'SPEED', 'X(LONG)', 'Y(LATI)', 'DirType', 'country']

conn = requests.session()
conn.headers.update(get_head)
r = conn.get('https://www.waze.com/row-Descartes/app/Session?language=ru')
conn.headers.update(post_head)
conn.headers['X-CSRF-Token'] = conn.cookies['_csrf_token']
r1 = conn.post('https://www.waze.com/login/create', data={'user_id':login, 'password':passw})
conn.headers.pop('X-CSRF-Token')
conn.headers.pop('Origin')
conn.headers.pop('Content-Type')

with open(output, "ab") as f:
    writer = csv.writer(f)
    writer.writerow(csv_head)

ids = []
n = 0

def parse(x):
        data['bbox'] = '%s,%s,%s,%s' % (x[0], x[1], x[2], x[3])
        r2 = conn.get('https://www.waze.com/row-Descartes/app/Features', params=data)
        global n
        n += 1
        try:
            cameras = r2.json()['cameras']['objects']
            try:
                country = r2.json()['countries']['objects'][0]['name']
            except:
                country = None
            for camera in cameras:
                if camera['validated']:
                    id = camera['id']
                    if id not in ids:
                        ids.append(id)
                        azymuth = camera['azymuth']
                        if camera['type'] == 2:
                            camera_type = 6
                        elif camera['type'] == 3:
                            camera_type = 11
                        elif camera['type'] == 4:
                            camera_type = 2
                        else:
                            camera_type = str(camera['type']) + '*non_changed*'
                        speed = camera['speed']
                        lon = camera['geometry']['coordinates'][0]
                        lat = camera['geometry']['coordinates'][1]
                        row = [id, azymuth, camera_type, speed, lon, lat, 1, country]
                        lock.acquire()
                        with open(output, "ab") as f:
                            writer = csv.writer(f)
                            writer.writerow(row)
                        lock.release()
        except Exception as e:
            print e
            print r2.text

def printer():
    global n, vsego
    while n != vsego:
        sys.stdout.write("Parsed areas: " + str(n) + '/' + str(vsego) + ' - (' + str((n / (vsego * 0.01))) + '%)' + "\r")
        time.sleep(0.1)

start = time.time()

arr = equal_areas(bott_x, bott_y, top_x, top_y, 70000, 37000)
vsego = len(arr)
print 'Divided into %d areas...' % vsego

lock = Lock()

pr = thread.start_new_thread(printer,())
pool = ThreadPool(threads)
pool.map(parse, arr)

pool.close()
pool.join()

print '\nTook time: %s sec' % str(time.time() - start)
raw_input('Press ENTER key to exit.')